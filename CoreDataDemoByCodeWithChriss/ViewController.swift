////  ViewController.swift
//  CoreDataDemoByCodeWithChriss
//
//  Created on 30/07/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // Reference to managed object context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var items: [Person]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Get items from CoreData
        fetchPeople()
    }
    
    func fetchPeople() {
        
        // Fetch the data from CoreData to display
        do {
            items = try context.fetch(Person.fetchRequest())
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch  {
        }
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        
        // Create alert
        let alert = UIAlertController(title: "Add Person", message: "What is their name?", preferredStyle: .alert)
        alert.addTextField()
        
        // Configure button handler
        let submitButton = UIAlertAction(title: "Add", style: .default) { _ in
            
            // Get the textfield for the alert
            let textField = alert.textFields![0]
            
            // Create a Person object
            let newPerson = Person(context: self.context)
            newPerson.name = textField.text
            newPerson.age = 10
            newPerson.gender = "M"
            
            // Save the data
            do {
                try self.context.save()
            } catch {
            }
            
            // Refresha the data
            self.fetchPeople()
        }
        
        // Add button
        alert.addAction(submitButton)
        
        // Show alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // Create swipe action
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
            
            // Which person to remove
            let personToRemove = self.items![indexPath.row]
            
            // Remove the person
            self.context.delete(personToRemove)
            
            // Save the data
            do {
                try self.context.save()
            } catch {
            }
            
            // Re-fetch the data
            self.fetchPeople()
        }
        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        // Get person from array and set table conten
        let person = items![indexPath.row]
        
        cell.textLabel?.text = person.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Selected person
        let person = items![indexPath.row]
        
        // Create alert
        let alert = UIAlertController(title: "Edit Person", message: "Edit name?", preferredStyle: .alert)
        alert.addTextField()
        
        let textField = alert.textFields![0]
        textField.text = person.name
        
        // Configure button handler
        let saveButton = UIAlertAction(title: "Save", style: .default) { (action) in
            let textField = alert.textFields![0]
            
            // Edit name of person
            person.name = textField.text
            
            // Save the data
            do {
                try self.context.save()
            } catch {
            }
            
            // Re-fetch the data
            self.fetchPeople()
        }
        
        // Add button
        alert.addAction(saveButton)
        
        // Show alert
        self.present(alert, animated: true, completion: nil)
    }
}


